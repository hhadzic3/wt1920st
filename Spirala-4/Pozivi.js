let Pozivi = (function () {

  function dajPodatkeJSON() {
    const url = "http://localhost:8080/zauzecaJSON";
    $.get(url, function (data) {
      let p = data.objekat.periodicna;
      let v = data.objekat.vanredna;
      console.log(p);
      console.log(v);
      Kalendar.ucitajPodatke(p, v);
    });
  }

  function dajPocetneSlike() {
    for (let br = 1; br <= 3 && br <= br_slika ; br++) {
      let imageDir = "http://localhost:8080/img/" + br ;
      $.get(imageDir, function (data) {
        var path = '/img/' + br +'.jpg';
        $('.slike').append(`<img class='imgg${br}' src=${path}>`);
      });
    }
  }
  
  function dajSljedeceSlike(brojac , pocetak , kraj , br_slika) {
    var broj = brojac;
    for (let brr = pocetak; brr <= kraj && brr <= br_slika; brr++) {
      let imageDir = "http://localhost:8080/img/" + brr + ".jpg";
      
      $.get(imageDir, function (data) {
        var path = '/img/' + brr +'.jpg';
        if($(`.imgg${brr}`).length == 0) $('.slike').append(`<img class='imgg${brr}' src=${path}>`);
        
        else $(`.imgg${brr}`).css({ display: "inline-flex" });
      });
      broj++;
    }
    return broj;
  }
  
  return {
    dajPodatke: dajPodatkeJSON,
    dajPocetne: dajPocetneSlike,
    dajSljedece: dajSljedeceSlike
  };
}());


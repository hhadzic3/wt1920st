let assert = chai.assert;
describe('Kalendar', function() {
    describe('iscrtajKalendar()', function() {
      it('should draw 30 days', function() {
        Kalendar.iscrtajKalendar("divCal", 10);
        let dani = document.getElementsByClassName("normal");
        assert.equal(30, dani.length,"Broj dana treba biti 30");
      });
      it('should draw 31 days', function() {
          Kalendar.iscrtajKalendar("divCal", 11);
          let dani = document.getElementsByClassName("normal");
        
          assert.equal(31, dani.length,"Broj dana treba biti 31");
        });
      it('1. dan u petak za trenutni mjesec', function() {
          Kalendar.iscrtajKalendar("divCal", 10);
          let daniPrijePrvog = document.getElementsByClassName("before");
          assert.equal(4, daniPrijePrvog.length,"Broj dana prije 1. treba biti 4 što znači da je 1. na petku");
        });
      it('Zadnje dan trenutnog mjeseca je u subotu', function() {
          Kalendar.iscrtajKalendar("divCal", 10);
          let daniPoslijeZadnjeg = document.getElementsByClassName("after");
          assert.equal(1, daniPoslijeZadnjeg.length,"Broj dana poslije zadnjeg dana treba biti 1 što znači da je zadnji dan subota");
        });
      it('Januar ima 31 dan i poćinje sa utorkom!?', function() {
          Kalendar.iscrtajKalendar("divCal", 0);
          let dani = document.getElementsByClassName("normal");
          let daniPrijePrvog = document.getElementsByClassName("before");
          assert.equal(1, daniPrijePrvog.length,"Broj dana prije 1. treba biti 1 što znači da je 1. utorak");
          assert.equal(31, dani.length,"Broj dana Januara");
        });
      it('Februar ima 28 dana i poćinje sa petkom!?', function() {
          Kalendar.iscrtajKalendar("divCal", 1);
          let dani = document.getElementsByClassName("normal");
          let daniPrijePrvog = document.getElementsByClassName("before");
          assert.equal( 4 , daniPrijePrvog.length, "Broj dana prije 1. treba biti 4 što znači da je 1. petak");
          assert.equal(28, dani.length,"Broj dana Februara");
        });
      it('April ima 30 dana i zavrsava sa utorkom!?', function() {
          Kalendar.iscrtajKalendar("divCal", 3);
          let dani = document.getElementsByClassName("normal");
          let daniPoslijeZadnjeg = document.getElementsByClassName("after");
          assert.equal(5, daniPoslijeZadnjeg.length,"Broj dana poslije zadnjeg dana treba biti 5 što znači da je zadnji dan utorak");
          assert.equal(30, dani.length,"Broj dana Aprila");
        });
    });
   });
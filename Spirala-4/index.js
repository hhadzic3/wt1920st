const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');

const Sequelize= require('sequelize');
const db = require(path.join( __dirname , 'moduli+db/db.js'));
db.sequelize.sync({force:false});

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(__dirname));

// ZADATAK 1
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/rezervacija.html');
});

app.get('/osoblje', (req, res) => {
    var jsonObj="";

    let hour = new Date().getHours() ;        // Get the hour (0-23);
    let minutes = new Date().getMinutes() ;      // Get the minutes (0-59)
    let vrijeme = hour ;
    let lokacija = 'kancelarija';
    let lok = [];
    let osobaKojaIspunjava = [];
    res.writeHead(200, {'Content-Type': 'application/json'});
    
    db.rezervacija.findAll({ attributes:['osobaId','terminRezervacijeId','salaId'] }).then(function(niz){
        
        for (let i = 0; i < niz.length; i++) {
            
            db.termin.findAll({
                attributes:['pocetak' , 'kraj'],
                where:{id : niz[i].dataValues.terminRezervacijeId } 
            }).then(function(nizTermina){
                for (let k = 0; k < nizTermina.length; k++) {
                
                    let flag = false;
                    let satiPoc = nizTermina[k].dataValues.pocetak.split(':')[0];
                    let satiKraj = nizTermina[k].dataValues.kraj.split(':')[0];
                    
                    if (satiPoc <= vrijeme && satiKraj >= vrijeme ) flag = true;

                    db.sala.findAll({
                            attributes:['naziv'],
                            where:{id : niz[i].dataValues.salaId } 
                        }).then(function(nizSala){
                            db.osoba.findAll().then(function(nizO){
                                
                                for(var j=0; j<nizO.length; j++){
                                    if (niz[i].dataValues.osobaId == nizO[j].dataValues.id && flag) {
                                        osobaKojaIspunjava.push(nizO[j].dataValues.id);
                                        lok.push(nizSala[0].dataValues.naziv);
                                    }
                                }
                                
                            }).then(function(){
                                db.osoba.findAll().then(function(nizO){  
                                    jsonObj+="{\"Osoblje\": [";
                                    for(var j=0; j<nizO.length; j++){
                                        let next = 0;

                                        jsonObj+=`{\"ime\":\"${nizO[j].ime}\",\"prezime\":\"${nizO[j].prezime}\",\"uloga\":\"${nizO[j].uloga}\"`;
                                        
                                        if (osobaKojaIspunjava.includes(nizO[j].dataValues.id)){
                                            jsonObj+=`,\"lokacija\":\"${lok[next]}\"}`;
                                            next++;
                                        }
                                        else jsonObj+=`,\"lokacija\":\"${lokacija}\"}`;
                                        
                                        if(j==nizO.length-1) jsonObj+=']}';
                                        else jsonObj+=','; 
                                    }
                                    res.end(jsonObj);
                                }).catch(function(err){
                                    console.log(err);
                                });
                            });
                        });
                    }   
                });
            }
    });

    
});


// salnje na lokaciju putem geta
app.get('/zauzecaJSON', (req, res) => {
    
    var jsonObj="";
    // PAZITI NA ZAREZ !! 
    db.termin.findAll().then(function(niz){
        jsonObj+="{\"objekat\": {";
        let p = 0;
        let v = 0;
        for(var i=0; i<niz.length; i++){
            if(niz[i].redovni == true) 
               p++;
            else v++; 
        }
        
        // sav periodicna
        for(var i=0; i<niz.length; i++){
            if (i == 0) jsonObj += "\"periodicna\" : [\n"
            let ima = false;
            if(niz[i].redovni == true){
                jsonObj+=`{\"dan\" : \"${niz[i].dan}\" , \"semestar\" : \"${niz[i].semestar}\" , \"pocetak\" : \"${niz[i].pocetak}\" , \"kraj\" : \"${niz[i].kraj}\" , \"naziv\" : \"1-11\" , \"predavac\" : \"Neko\" }`;
                ima = true;
            }
            if(i==niz.length-1) jsonObj+=']';
            else if ( i != niz.length-1 && ima && p!= 1){ jsonObj+=',\n'; p--;} 
        }
        
        // sva vanredna:
        for(var i=0; i<niz.length; i++){
            if (i == 0) jsonObj += ",\n\"vanredna\" : ["
            let ima = false;
            if(niz[i].redovni == false){
            jsonObj+=`{\"datum\" : \"${niz[i].datum}\" , \"pocetak\" : \"${niz[i].pocetak}\" , \"kraj\" : \"${niz[i].kraj}\" , \"naziv\" : \"1-11\" , \"predavac\" : \"Neko\" }`;
            ima = true;
            }
            if(i==niz.length-1) jsonObj+=']';
            else if ( i != niz.length-1 && ima && v!= 0){ jsonObj+=',\n'; v--;} 
        }
        jsonObj+=' } }';
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(jsonObj);
        
    }).catch(function(err){
        console.log(err);
    });  
});
// ZADATAK 2 -> citanje i pisanje iz baze
app.post('/z2', function (req, res) {
    let tijelo = req.body;
    let id = 1;
    if (tijelo['dan'] != undefined) {
        db.termin.create ({                
            redovni: true,
            dan: tijelo['dan'],
            datum: null,
            semestar: tijelo['semestar'],
            pocetak: tijelo['pocetak'],
            kraj: tijelo['kraj'],
            }).then(function(insertedData) {
                id = insertedData.dataValues.id;
                db.osoba.findAll({
                    attributes:['id'],
                    where:{ime : tijelo['predavac']} 
                }).then(function(nizOsobe){
                    db.sala.findAll({
                        attributes:['id'],
                        where:{naziv : tijelo['naziv']} 
                    }).then(function(nizSale){
                        db.rezervacija.create({
                            osobaId: nizOsobe[0].dataValues.id,
                            terminRezervacijeId: id,
                            salaId: nizSale[0].dataValues.id
                        }).then( (insertedData)=>{
                            console.log(insertedData.dataValues);
                        });
                    });
                });
        
            });
        } else {
            db.termin.create ({                
                redovni: false,
                dan: null,
                datum: tijelo['datum'],
                semestar: null,
                pocetak: tijelo['pocetak'],
                kraj: tijelo['kraj'],
            }).then(function(insertedData) {
                id = insertedData.dataValues.id;
                db.osoba.findAll({
                    attributes:['id'],
                    where:{ime : tijelo['predavac']} 
                }).then(function(nizOsobe){
                    db.sala.findAll({
                        attributes:['id'],
                        where:{naziv : tijelo['naziv']} 
                    }).then(function(nizSale){
                        db.rezervacija.create({
                            osobaId: nizOsobe[0].dataValues.id,
                            terminRezervacijeId: id,
                            salaId: nizSale[0].dataValues.id
                        }).then( (insertedData)=>{
                            console.log(insertedData.dataValues);
                        });
                    });
                });
        
            });
        }

       
    if (tijelo['dan'] != undefined) {
        res.json({
            dan: tijelo['dan'],
            semestar: tijelo['semestar'],
            pocetak: tijelo['pocetak'],
            kraj: tijelo['kraj'],
            naziv: tijelo['naziv'],
            predavac: tijelo['predavac']
        });
    } else {
        res.json({
            datum: tijelo['datum'],
            pocetak: tijelo['pocetak'],
            kraj: tijelo['kraj'],
            naziv: tijelo['naziv'],
            predavac: tijelo['predavac']
        });
    }

});


app.get('/saleJSON' , (req , res) => {
    var jsonObj = "";
    db.sala.findAll().then(function(niz){  
        jsonObj+="{\"Sale\": [";
        for(var j=0; j<niz.length; j++){
            jsonObj+=`{\"naziv\":\"${niz[j].naziv}\"}`;
            if(j==niz.length-1) jsonObj+=']}';
            else jsonObj+=','; 
        }
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(jsonObj);
    });
});


// Spirala3 ->galerija
var imageDir = __dirname + "/img/";
app.get("/img/:id", function (request, response) {
    var path = imageDir + request.params.id + '.jpg';
    console.log("fetching image: ", path);
    response.sendFile(path);
});


module.exports = app.listen(8080, () => {
    console.log('Server radi...');
});
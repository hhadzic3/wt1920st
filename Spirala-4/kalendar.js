let Kalendar = (function () {
  function dodajKlasuZauzet(s) {
    $(s).addClass('zauzet');
  }

  function skloniKlasuZauzet(s) {
    $(s).removeClass('zauzet');
  }

  var periodicna = [];
  var vanredna = [];

  function ucitajPodatkeImpl(p, v) {
    periodicna = p;
    vanredna = v;
  }

  function provjeriSateIMinute(satiPocetka, satiPocetka2, satiKraja, satiKraja2) {
    if (satiPocetka == satiPocetka2 && satiKraja == satiKraja2) return true; // 12 do 2
    if (satiPocetka2 <= satiPocetka && satiKraja > satiKraja2 && satiKraja2 > satiPocetka) return true; //12 do 1 ili 11 do 1
    if (satiPocetka2 < satiKraja && satiKraja2 >= satiKraja && satiKraja2 > satiPocetka2) return true; // 1 do 3
    if (satiPocetka2 < satiPocetka && satiKraja2 > satiKraja) return true; // 11 do 3 

    return false;
  }


  var Cal = function (divId) {
    this.divId = divId;
    this.DaysOfWeek = ['Pon', 'Uto', 'Sri', 'Čet', 'Pet', 'Sub', 'Ned'];
    this.Months = ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Juni', 'Juli', 'August', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'];

    var d = new Date();

    this.currMonth = d.getMonth();
    this.currYear = d.getFullYear();
    this.currDay = d.getDate();
  };

  Cal.prototype.nextMonth = function () {
    if (this.currMonth != 11)
      this.currMonth++;

    this.showcurr();
  };

  Cal.prototype.previousMonth = function () {
    if (this.currMonth != 0) this.currMonth--;
    this.showcurr();
  };

  Cal.prototype.showcurr = function () {
    this.showMonth(this.currYear, this.currMonth);
  };

  Cal.prototype.showMonth = function (y, m) {
    var d = new Date();
    var firstDayOfMonth = new Date(y, m, 0).getDay();
    var lastDateOfMonth = new Date(y, m + 1, 0).getDate();
    var lastDayOfLastMonth = m == 0 ? new Date(y - 1, 11, 0).getDate() : new Date(y, m, 0).getDate();

    var html = '<table>';

    // Ispisi mjesec
    html += '<thead><tr>';
    html += '<td colspan="7" id="nazivM" >' + this.Months[m] + '</td>';
    html += '</tr></thead>';

    // Ispisi dane u sedmici
    html += '<tr class="days">';
    for (var i = 0; i < this.DaysOfWeek.length; i++) {
      html += '<td>' + this.DaysOfWeek[i] + '</td>';
    }
    html += '</tr>';

    // Ispis dana
    var i = 1;
    do {
      var dow = new Date(y, m, i - 1).getDay();
      // Ako je pon ispisi u novi red
      if (dow == 0) {
        html += '<tr>';
      }
      // Ako nije ponedjeljak a prvi dan u mjesecu  -> ispis zadnjih dana iz mjeseca
      else if (i == 1) {
        html += '<tr>';
        var k = lastDayOfLastMonth - firstDayOfMonth + 1;
        for (var j = 0; j < firstDayOfMonth; j++) {
          html += '<td class="not-current before" >' + k + '</td>';
          k++;
        }
      }

      // Dani trenutnog mjeseca
      var chk = new Date();
      var chkY = chk.getFullYear();
      var chkM = chk.getMonth();
      if (m == 0 || m == 9 || m == 10 || m == 11)
        html += '<td class="normal zimski" id="m' + (m + 1) + '" >' + '<button id="b' + i + '" >' + i + '</button>' + '</td>';
      else if (m == 1 || m == 2 || m == 3 || m == 4 || m == 5)
        html += '<td class="normal ljetni" id="m' + (m + 1) + '" >' + '<button id="b' + i + '" >' + i + '</button>' + '</td>';
      else html += '<td class="normal" id="m' + (m + 1) + '" >' + '<button id="b' + i + '" >' + i + '</button>' + '</td>';

      // If Nedjelja, closes the row
      if (dow == 6) {
        html += '</tr>';
      }
      // If not Nedjelja, ali zadnji dan selektovanog mjeseca -> sljedeci par dana novog mjeseca
      else if (i == lastDateOfMonth) {
        var k = 1;
        for (dow; dow < 6; dow++) {
          html += '<td class="not-current after">' + k + '</td>';
          k++;
        }
      }
      i++;
    } while (i <= lastDateOfMonth);
    html += '</table>';
    document.getElementById(this.divId).innerHTML = html;
  };

  function iscrtajKalendarImpl(kalendarRef, mjesec) {
    var c = new Cal(kalendarRef);
    let trenutnaGodina = 2019;
    c.showMonth(trenutnaGodina, mjesec);

    getId('btnNext').onclick = function () {
      c.nextMonth();
      Kalendar.ucitajPodatke(periodicna, vanredna);
      if (mjesec != 11) trenutniMjesec++;
    };

    getId('btnPrev').onclick = function () {
      c.previousMonth();
      Kalendar.ucitajPodatke(periodicna, vanredna);
      if (mjesec != 0) trenutniMjesec--;
    };
  }

  // OBOJI ZAUZECAAAA
  function obojiZauzecaVanredna(mjesec, sala, pocetak, kraj) {
    var flag;
    var s = "";
    for (let i = 0; i < vanredna.length; i++) {

      var d = (vanredna[i].datum).split(".");
      let dan = parseInt(d[0]);
      let m = parseInt(d[1]);

      var poc = vanredna[i].pocetak.split(':');
      var poc2 = pocetak.split(':');

      var k = vanredna[i].kraj.split(':');
      var k2 = kraj.split(':');

      var h = parseInt(poc[0]);
      var h2 = parseInt(poc2[0]);

      var hkraj = parseInt(k[0]);
      var hkraj2 = parseInt(k2[0]);

      s = "#m" + m + " #b" + dan;
      if (mjesec == m && provjeriSateIMinute(h, h2, hkraj, hkraj2) === true && sala == vanredna[i].naziv) {
        dodajKlasuZauzet(s);
        flag = true;
      } else {
        skloniKlasuZauzet(s);
        flag = false
      }
    }
    
    if (flag) {
      let poruka = `Nije moguće rezervisati salu ${sala} za navedeni datum ${mjesec}/2019 od ${pocetak} do ${kraj} jer je osoba Neko rezervisala prije vas!`;
      alert(poruka);
    } else {
      let ddd = 1;
      let pred = getId('osoblje').value;
      
      if (confirm("Da li ste sigurni da želite zauzeti salu u naznačenom terminu?\nKliknite OK ukoliko želite, Cancel u suprotnom.")) {
        dodajKlasuZauzet(s);
        alert('Klikom na dan odaberite koji dan zelite rezervisati!');
        $("tr td button").click(function (event) {
          let i = 1;
          while (i <= 31) {
            if ($(this).prop("id") != ("b" + i)) {
              ddd++;
            } else break;
            i++;
          }
        $.ajax({
          url: '/z2',
          type: "POST",
          data: {
            datum: `${ddd}.${mjesec}.2019`,
            pocetak: pocetak,
            kraj: kraj,
            naziv: sala,
            predavac: pred
          },
          success: function (data, status, settings) {
            dodajKlasuZauzet("#m" + mjesec + " #b" + ddd);
            alert(`Uspješno ste rezervisali ${sala} za datum ${ddd}/${mjesec}/2019 od ${pocetak} do ${kraj} sati!`);
            Pozivi.dajPodatke();
          },
          error: function (ajaxrequest, ajaxOptions, thrownError) {
            alert("error");
          }
        });
      });
      }
    }

  }

  function obojiZauzecaPeriodicna(mjesec, sala, pocetak, kraj) {
    var flag;
    var s2 = "";
    for (let i = 0; i < periodicna.length; i++) {
      var d2 = periodicna[i].dan;
      s2 = "." + periodicna[i].semestar + " #b" + d2;
      var m = [];
      if (periodicna[i].semestar == 'zimski') {
        m = [10, 11, 12, 1];
      } else if (periodicna[i].semestar == 'ljetni') {
        m = [3, 4, 5, 6];
      }

      var poc = periodicna[i].pocetak.split(':');
      var poc2 = pocetak.split(':');
      var h = parseInt(poc[0]);
      var h2 = parseInt(poc2[0]);

      var k = periodicna[i].kraj.split(':');
      var k2 = kraj.split(':');
      var hkraj = parseInt(k[0]);
      var hkraj2 = parseInt(k2[0]);

      if (m.includes(mjesec) && provjeriSateIMinute(h, h2, hkraj, hkraj2) === true && sala == periodicna[i].naziv) {
        dodajKlasuZauzet(s2);
        while(d2 < 31){
          d2 += 7;
          dodajKlasuZauzet("." + periodicna[i].semestar + " #b" + d2);
        }
        while(d2 > 0){
          d2 -= 7;
          dodajKlasuZauzet("." + periodicna[i].semestar + " #b" + d2);
        }
        flag = true;
      } else {
        skloniKlasuZauzet(s2);
        while(d2 < 31){
          d2 += 7;
          skloniKlasuZauzet("." + periodicna[i].semestar + " #b" + d2);
        }
        while(d2 > 0){
          d2 -= 7;
          skloniKlasuZauzet("." + periodicna[i].semestar + " #b" + d2);
        }
        flag = false;
      }
    }
    var sem = "";
    if (mjesec == 10 || mjesec == 10 || mjesec == 11 || mjesec == 12 || mjesec == 1)
    sem = 'zimski'
    else if (mjesec == 5 || mjesec == 6 || mjesec == 3 || mjesec == 4)
    sem = "ljetni";

    if (flag) {
      let poruka = `Nije moguće rezervisati ${sala} za ${sem} semestar u terminu ${pocetak}-${kraj} jer je osoba Neko rezervisala prije!`;
      alert(poruka);
    } else {
      let pred = getId('osoblje').value;
      
      if (confirm("Da li ste sigurni da želite zauzeti salu u naznačenom terminu?\nKliknite OK ukoliko želite, Cancel u suprotnom.")) {
        dodajKlasuZauzet(s2);
        while(d2 < 31){
          d2 += 7;
          dodajKlasuZauzet("." + sem + " #b" + d2);
        }
        while(d2 > 0){
          d2 -= 7;
          dodajKlasuZauzet("." + sem + " #b" + d2);
        }


        alert('Klikom na dan odaberite koji dan zelite rezervisati!');
        $("tr td button").click(function (event) {
          let dd = 1;
          let i = 1;
          while (i <= 31) {
            if ($(this).prop("id") != ("b" + i)) {
              dd++;
            } else break;
            i++;
          }
      
        $.ajax({
          url: '/z2',
          type: "POST",
          data: {
            dan: dd,
            semestar: sem,
            pocetak: pocetak,
            kraj: kraj,
            naziv: sala,
            predavac: pred
          },
          success: function (data, status, settings) {
            dodajKlasuZauzet("." + sem + " #b" + dd);
            while(dd < 31){
              dd += 7;
              dodajKlasuZauzet("." + sem + " #b" + dd);
            }
            while(dd > 0){
              dd -= 7;
              dodajKlasuZauzet("." + sem + " #b" + dd);
            }
            alert(`Uspješno ste rezervisali salu ${sala} za ${sem} semestar u terminu od ${pocetak} do
            ${kraj}!`);
            Pozivi.dajPodatke();
          },
          error: function (ajaxrequest, ajaxOptions, thrownError) {
            alert("error");
          }
        });
      });
      }
    }
  }

  function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
    if (document.getElementById("checkbox1").checked)
      obojiZauzecaPeriodicna(mjesec, sala, pocetak, kraj);
    
      else obojiZauzecaVanredna(mjesec, sala, pocetak, kraj);
  }
  return {
    obojiZauzeca: obojiZauzecaImpl,
    ucitajPodatke: ucitajPodatkeImpl,
    iscrtajKalendar: iscrtajKalendarImpl
  }
}());

let trenutniMjesec = 1;
window.onload = function () {
  Kalendar.iscrtajKalendar("divCal", 0);
  $('#checkbox1').prop('checked', true);
  getId('pocetak').value = "12:00";
  getId('kraj').value = "14:00";
}

function getId(id) {
  return document.getElementById(id);
}

function validirajVrijeme(pocetak, kraj) {
  var poc2 = pocetak.split(':');
  var k2 = kraj.split(':');
  var h2 = parseInt(poc2[0]);
  var hkraj2 = parseInt(k2[0]);
  if (h2 >= hkraj2) {
    window.alert('Početak ne smiji biti veći ili jednak kraju zauzeća');
    return false;
  } else return true;
}

$('#submit').click(function () {
  var sala = getId('sale').value;
  var poc = getId('pocetak').value;
  var kr = getId('kraj').value;
  if (validirajVrijeme(poc, kr))
    Kalendar.obojiZauzeca(document.getElementById("divCal"), trenutniMjesec, sala, poc, kr);
});
const Sequelize = require("sequelize");
const sequelize = new Sequelize("dbwt19","root", "root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.osoba = sequelize.import(__dirname+'/osoblje.js');
db.rezervacija = sequelize.import(__dirname+'/rezervacije.js');
db.sala = sequelize.import(__dirname+'/sala.js');
db.termin = sequelize.import(__dirname+'/termini.js');

//relacije
// Veza 1-n 
db.osoba.hasMany(db.rezervacija,{as:'rezervacijeOsobe'});

// Veza 1-1 
db.termin.hasOne(db.rezervacija,{as:'terminRezervacije'});

// Veza 1-n 
db.sala.hasMany(db.rezervacija, {as:'rezervacijaSale' } );

// Veza 1-1 
db.osoba.hasOne(db.sala, {as:'saleOsoba' } );

module.exports=db;
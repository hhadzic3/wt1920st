const db = require('./db.js')

db.sequelize.sync({
    force: true
}).then(function () {
    inicializacija().then(function () {
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });
});

function inicializacija() {
    
    var osobeListaPromisea = [];
    var rezervacijeListaPromisea = [];
    var saleListaPromisea = [];
    var terminiListaPromisea = [];

    return new Promise(function (resolve, reject) {

        osobeListaPromisea.push(db.osoba.create({
            ime: 'Neko',
            prezime: 'Nekic',
            uloga: 'profesor'
        }));
        osobeListaPromisea.push(db.osoba.create({
            ime: 'Drugi',
            prezime: 'Neko',
            uloga: 'asistent'
        }));
        osobeListaPromisea.push(db.osoba.create({
            ime: 'Test',
            prezime: 'Test',
            uloga: 'asistent'
        }));

        terminiListaPromisea.push(db.termin.create({
            redovni: true,
            dan: 0,
            datum: null,
            semestar: "zimski",
            pocetak: '13:00',
            kraj: '14:00' 
        }));
        terminiListaPromisea.push(db.termin.create({
            redovni: false,
            dan: null,
            datum: '01.01.2020',
            semestar: null,
            pocetak: '12:00',
            kraj: '13:00' 
        }));


        Promise.all(osobeListaPromisea).then(function (osobe) {
            var neko = osobe.filter(function (a) {
                return a.ime === 'Neko'
            })[0];
            var drugi = osobe.filter(function (a) {
                return a.ime === 'Drugi'
            })[0];
            var test = osobe.filter(function (a) {
                return a.ime === 'Test'
            })[0];
            
            saleListaPromisea.push(
                db.sala.create({
                    naziv:"1-11",
                    //zaduzenaOsoba: 1 // ne zna se dal treba
                    saleOsobaId: 1
                }).then(function (k) {
                    //k.setSaleOsoba([neko]);
                    return new Promise(function (resolve, reject) {
                        resolve(k);
                    });
                })
            );

                saleListaPromisea.push(
                    db.sala.create({
                        naziv:"1-15",
                        saleOsobaId: 2
                    }).then(function (k) {
                        //k.setSaleOsoba([drugi]);
                        return new Promise(function (resolve, reject) {
                            resolve(k);
                        });
                    })
                );
                    
                    
                    
                Promise.all(saleListaPromisea).then(function (sale) {
                    
                    var jedanS = sale.filter(function (k) {
                        return k.naziv === '1-11'
                    })[0];
                    var dvaS = sale.filter(function (k) {
                        return k.naziv === '1-15'
                    })[0];
                    
                    
                    
                    Promise.all(terminiListaPromisea).then(function (termini) {
                        
                        var jedanT = termini.filter(function (k) {
                            return k.datum === '01.01.2020'
                        })[0];
                        var dvaT = termini.filter(function (k) {
                            return k.datum === null
                        })[0];
                        
                    rezervacijeListaPromisea.push(
                        db.rezervacija.create({
                            osobaId : 1,
                            terminRezervacijeId: 1,
                            salaId: 1
                        }).then(function (k) {
                           /* k.setRezervacijeOsobe([neko]);
                            k.setTerminRezervacije([jedanT]);
                            k.setRezervacijaSale([jedanS]);*/
                            return new Promise(function (resolve, reject) {
                                resolve(k);
                            });
                        })
                    );
                
                    rezervacijeListaPromisea.push(
                        db.rezervacija.create({
                            osobaId : 3,
                            terminRezervacijeId: 2,
                            salaId: 1
                        }).then(function (k) {
                            /*k.setRezervacijeOsobe([test]);
                            k.setTerminRezervacije([dvaT]);
                            k.setRezervacijaSale([jedanS]);*/
                            return new Promise(function (resolve, reject) {
                                resolve(k);
                            });
                        })
                    );
                
        

                    Promise.all(rezervacijeListaPromisea).then(function (b) {
                        resolve(b);
                    }).catch(function (err) {
                        console.log("Rezervacije greska " + err);
                    });
                    

                }).catch(function (err) {
                    console.log("Termini greska " + err);
                });
            
            }).catch(function (err) {
                console.log("Sale greska " + err);
            });

        }).catch(function (err) {
            console.log("Osobe greska " + err);
        });

    });
}
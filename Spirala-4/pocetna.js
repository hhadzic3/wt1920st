const br_slika = 10;
let brojac = 3;
let brojac2 = 3;

Pozivi.dajPocetne(br_slika);

function prethodne(pocetak , kraj){
   if (kraj + 1 <= br_slika) $(`.imgg${kraj + 1 }`).css({ display: "none" });
   if (kraj + 2 <= br_slika) $(`.imgg${kraj + 2 }`).css({ display: "none" });
   if (kraj + 3 <= br_slika) $(`.imgg${kraj+3}`).css({ display: "none" });

    $(`.imgg${pocetak }`).css({ display: "inline-flex" });
    $(`.imgg${pocetak + 1 }`).css({ display: "inline-flex" });
    $(`.imgg${ kraj }`).css({ display: "inline-flex" });
    
    if (brojac % 3 == 1) brojac -= 1;
    else if (brojac % 3 == 2) brojac -= 2;
    else brojac-=3;
}

function sljedece(pocetak , kraj) {
    $(`.imgg${pocetak-1}`).css({ display: "none" });
    $(`.imgg${pocetak-2}`).css({ display: "none" });
    $(`.imgg${pocetak-3}`).css({ display: "none" });
    
    brojac = Pozivi.dajSljedece(brojac , pocetak , kraj , br_slika);
}

$('#sljedece').click(function () {
    if (brojac < br_slika) sljedece(brojac+1 , brojac+3);
});

$('#prethodne').click(function () {
    if (brojac % 3 == 1 && brojac > 3) prethodne(brojac - 3 , brojac - 1 );
    else if (brojac % 3 == 2 && brojac > 3) prethodne(brojac - 4 , brojac - 2 );
    else if (brojac > 3) prethodne(brojac - 5 , brojac - 3 );
    console.log(brojac);
});
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');
chai.should();

chai.use(chaiHttp);

describe('Testiranje servera', function() {
    
    describe('GET/osoblje', ()=> {
        it('da li get radi kako treba', (done) => {
            chai.request(app)
            .get('/osoblje')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            });
        });
        it('Get sve osobe', (done) => {
            chai.request(app)
            .get('/osoblje')
            .end((err, res) => {
                res.body.Osoblje.length.should.have.eq(3);
                res.body.Osoblje[0].should.have.property('ime');
                res.body.Osoblje[0].should.have.property('prezime');
                res.body.Osoblje[0].should.have.property('uloga');
                res.body.Osoblje[0].should.have.property('lokacija');
                done();
            });
        });
        it('Da li se dohvati dovoljan broj osoba', (done) => {
            chai.request(app)
            .get('/osoblje')
            .end((err, res) => {
                res.body.Osoblje.length.should.have.eq(3);
                done();
            });
        });
    });

    describe('Dohvaćanje zauzeća', ()=> {
        it('da li dohvatanje radi kako treba', (done) => {
            chai.request(app)
            .get('/zauzecaJSON')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            });
        });

        it('dohvati sve termine Periodicna', (done) => {
            chai.request(app)
            .get('/zauzecaJSON')
            .end((err, res) => {
                res.body.objekat.periodicna[0].should.have.property('dan');
                res.body.objekat.periodicna[0].should.have.property('semestar');
                res.body.objekat.periodicna[0].should.have.property('pocetak');
                res.body.objekat.periodicna[0].should.have.property('kraj');
                res.body.objekat.periodicna[0].should.have.property('naziv');
                res.body.objekat.periodicna[0].should.have.property('predavac');
                done();
            });
        });
        it('dohvati sve termine Vanredna', (done) => {
            chai.request(app)
            .get('/zauzecaJSON')
            .end((err, res) => {
                res.body.objekat.vanredna[0].should.have.property('datum');
                res.body.objekat.vanredna[0].should.have.property('pocetak');
                res.body.objekat.vanredna[0].should.have.property('kraj');
                res.body.objekat.vanredna[0].should.have.property('naziv');
                res.body.objekat.vanredna[0].should.have.property('predavac');
                done();
            });
        });
    });

    describe('Dohvaćanje sala', ()=> {
        it('da li dobar status', (done) => {
            chai.request(app)
            .get('/saleJSON')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });
        it('da li se dohvataju objekti status', (done) => {
            chai.request(app)
            .get('/saleJSON')
            .end((err, res) => {
                res.body.should.be.a('object');
                done();
            });
        });

        it('dohvati sve sale', (done) => {
            chai.request(app)
            .get('/saleJSON')
            .end((err, res) => {
                res.body.Sale[0].should.have.property('naziv');
                done();
            });
        });

        it('dohvati sve sale', (done) => {
            chai.request(app)
            .get('/saleJSON')
            .end((err, res) => {
                res.body.Sale.length.should.have.eq(2);
                done();
            });
        });

    });
    
});